use std::collections::HashMap;
use std::fs;

fn main() {
    let contents = fs::read_to_string("./total_cases.csv").expect("Ups");
    let mut lines = contents.lines();

    let paises: Vec<&str> = lines.next().unwrap().split(",").collect();
    let muertes: Vec<&str> = lines.last().unwrap().split(",").collect();

    let mut datos = HashMap::new();

    for (i, item) in paises.iter().enumerate() {
        datos.insert(item, muertes[i]);
    }

    let united = datos[&"United States"];
    datos.insert(&"United States of America", united);

    datos.remove(&"United States");
    datos.remove(&"date");
    datos.remove(&"World");

    println!("Country,Deaths");
    for (key, val) in datos.iter() {
        if val != &"" {
            println!("{},{}", key, val);
        }
    }
}
